﻿namespace CubeWorldTrainer
{
    partial class TeleportWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TeleportWindow));
            this.teleportlist = new CubeWorldTrainer.controls.EditableListbox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resetbtn = new System.Windows.Forms.Button();
            this.addbtn = new System.Windows.Forms.Button();
            this.teleporttextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.loadbtn = new System.Windows.Forms.Button();
            this.savebtn = new System.Windows.Forms.Button();
            this.delbtn = new System.Windows.Forms.Button();
            this.backupbtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.syncbtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // teleportlist
            // 
            this.teleportlist.BackColor = System.Drawing.Color.Black;
            this.teleportlist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.teleportlist.Editable = true;
            this.teleportlist.ForeColor = System.Drawing.Color.White;
            this.teleportlist.FormattingEnabled = true;
            this.teleportlist.Location = new System.Drawing.Point(3, 33);
            this.teleportlist.Name = "teleportlist";
            this.teleportlist.Size = new System.Drawing.Size(134, 158);
            this.teleportlist.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.resetbtn);
            this.groupBox1.Controls.Add(this.addbtn);
            this.groupBox1.Controls.Add(this.teleporttextbox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(143, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 89);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "manage";
            // 
            // resetbtn
            // 
            this.resetbtn.AutoSize = true;
            this.resetbtn.BackColor = System.Drawing.Color.Black;
            this.resetbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.resetbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetbtn.Location = new System.Drawing.Point(148, 44);
            this.resetbtn.Name = "resetbtn";
            this.resetbtn.Size = new System.Drawing.Size(42, 25);
            this.resetbtn.TabIndex = 3;
            this.resetbtn.Text = "reset";
            this.resetbtn.UseVisualStyleBackColor = false;
            this.resetbtn.Click += new System.EventHandler(this.Resetbtn_Click);
            // 
            // addbtn
            // 
            this.addbtn.AutoSize = true;
            this.addbtn.BackColor = System.Drawing.Color.Black;
            this.addbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.addbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addbtn.Location = new System.Drawing.Point(196, 44);
            this.addbtn.Name = "addbtn";
            this.addbtn.Size = new System.Drawing.Size(42, 25);
            this.addbtn.TabIndex = 2;
            this.addbtn.Text = "Add";
            this.addbtn.UseVisualStyleBackColor = false;
            this.addbtn.Click += new System.EventHandler(this.Addbtn_Click);
            // 
            // teleporttextbox
            // 
            this.teleporttextbox.Location = new System.Drawing.Point(48, 18);
            this.teleporttextbox.Name = "teleporttextbox";
            this.teleporttextbox.Size = new System.Drawing.Size(190, 20);
            this.teleporttextbox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "name:";
            // 
            // loadbtn
            // 
            this.loadbtn.AutoSize = true;
            this.loadbtn.BackColor = System.Drawing.Color.Black;
            this.loadbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.loadbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadbtn.ForeColor = System.Drawing.Color.White;
            this.loadbtn.Location = new System.Drawing.Point(266, 204);
            this.loadbtn.Name = "loadbtn";
            this.loadbtn.Size = new System.Drawing.Size(58, 25);
            this.loadbtn.TabIndex = 4;
            this.loadbtn.Text = "Load list";
            this.loadbtn.UseVisualStyleBackColor = false;
            this.loadbtn.Click += new System.EventHandler(this.Loadbtn_Click);
            // 
            // savebtn
            // 
            this.savebtn.AutoSize = true;
            this.savebtn.BackColor = System.Drawing.Color.Black;
            this.savebtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.savebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savebtn.ForeColor = System.Drawing.Color.White;
            this.savebtn.Location = new System.Drawing.Point(202, 204);
            this.savebtn.Name = "savebtn";
            this.savebtn.Size = new System.Drawing.Size(58, 25);
            this.savebtn.TabIndex = 5;
            this.savebtn.Text = "save list";
            this.savebtn.UseVisualStyleBackColor = false;
            this.savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // delbtn
            // 
            this.delbtn.AutoSize = true;
            this.delbtn.BackColor = System.Drawing.Color.Black;
            this.delbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.delbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delbtn.ForeColor = System.Drawing.Color.White;
            this.delbtn.Location = new System.Drawing.Point(103, 204);
            this.delbtn.Name = "delbtn";
            this.delbtn.Size = new System.Drawing.Size(93, 25);
            this.delbtn.TabIndex = 6;
            this.delbtn.Text = "Delete selected";
            this.delbtn.UseVisualStyleBackColor = false;
            this.delbtn.Click += new System.EventHandler(this.Delbtn_Click);
            // 
            // backupbtn
            // 
            this.backupbtn.AutoSize = true;
            this.backupbtn.BackColor = System.Drawing.Color.Black;
            this.backupbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.backupbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backupbtn.ForeColor = System.Drawing.Color.White;
            this.backupbtn.Location = new System.Drawing.Point(330, 204);
            this.backupbtn.Name = "backupbtn";
            this.backupbtn.Size = new System.Drawing.Size(58, 25);
            this.backupbtn.TabIndex = 7;
            this.backupbtn.Text = "Backup";
            this.backupbtn.UseVisualStyleBackColor = false;
            this.backupbtn.Click += new System.EventHandler(this.Backupbtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(365, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10);
            this.label2.Size = new System.Drawing.Size(35, 33);
            this.label2.TabIndex = 8;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CubeWorldTrainer.Properties.Resources.cb;
            this.pictureBox1.InitialImage = global::CubeWorldTrainer.Properties.Resources.cb;
            this.pictureBox1.Location = new System.Drawing.Point(143, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(245, 63);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // syncbtn
            // 
            this.syncbtn.AutoSize = true;
            this.syncbtn.BackColor = System.Drawing.Color.Black;
            this.syncbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.syncbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.syncbtn.ForeColor = System.Drawing.Color.White;
            this.syncbtn.Location = new System.Drawing.Point(4, 204);
            this.syncbtn.Name = "syncbtn";
            this.syncbtn.Size = new System.Drawing.Size(59, 25);
            this.syncbtn.TabIndex = 10;
            this.syncbtn.Text = "Sync";
            this.syncbtn.UseVisualStyleBackColor = false;
            this.syncbtn.Click += new System.EventHandler(this.Syncbtn_Click);
            // 
            // TeleportWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.teleportmanager1;
            this.ClientSize = new System.Drawing.Size(400, 241);
            this.Controls.Add(this.syncbtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.backupbtn);
            this.Controls.Add(this.delbtn);
            this.Controls.Add(this.savebtn);
            this.Controls.Add(this.loadbtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.teleportlist);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TeleportWindow";
            this.Text = "Teleport manager";
            this.Load += new System.EventHandler(this.TeleportWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private controls.EditableListbox teleportlist;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox teleporttextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addbtn;
        private System.Windows.Forms.Button resetbtn;
        private System.Windows.Forms.Button loadbtn;
        private System.Windows.Forms.Button savebtn;
        private System.Windows.Forms.Button delbtn;
        private System.Windows.Forms.Button backupbtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button syncbtn;
    }
}
﻿using CubeWorldTrainer.cheats;
using CubeWorldTrainer.controls;
using CubeWorldTrainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    public partial class Window : Form
    {

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        private Random rand = new Random();
        private System.Windows.Forms.Timer timer;
        private bool isMouseClicked = false;
        private Point lastLocation;
        private TrainerHelp helpwindw = new TrainerHelp();
        private TeleportWindow teleportwindw;
        private TrainerScan trainerscan = new TrainerScan();
        private Trainer trainer;
        private HotKeyProvider hotkey;


        private Cursor ungrab;
        private Cursor grab;

        private Mem m = new Mem();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public Window(bool debugg = false)
        {
            InitializeComponent();

            this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());

            this.Opacity = 0.0;
            this.timer = new System.Windows.Forms.Timer();
            this.timer.Interval = 100;
            this.timer.Tick += new EventHandler(tickevent);
            this.timer.Start();
        }

        private void Window2_Load(object sender, EventArgs e)
        {

            this.godmodecheck.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.manacheck.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.skillcheck.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.applecheck.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.insanitymode.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.freezetimecheckbox.CustomCheckBoxChange += new EventHandler(DoCheckboxEvent);
            this.flycheckbox.CustomCheckBoxChange += new EventHandler(DoFlyCheckBoxEvent);

            this.setDayToolStripMenuItem.Click += new EventHandler(this.SetDayToolStripMenuItem_Click);
            this.setNightToolStripMenuItem.Click += new EventHandler(this.SetNightToolStripMenuItem_Click);

            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            this.version.Text = String.Format(this.version.Text, Application.ProductVersion);
            this.cursorholder.Cursor = this.ungrab;
            this.makeMoveAble(this.cursorholder);
            this.FormClosing += new FormClosingEventHandler(onformclose);

            this.RegisterHotKeys();

            this.trainer = new Trainer(this, m);
            trainer.Start();
            this.teleportwindw = new TeleportWindow(this);
            TeleportRegistry.GetRegistry().LoadFromFile();
            foreach(TeleportLocation l in TeleportRegistry.GetRegistry().Values())
            {
                this.teleportbox.Items.Add(l.GetName());
            }
            if(teleportbox.Items.Count > 0)
            {
                this.teleportbox.SelectedIndex = 0;
            }
            
        }

        private void DoCheckboxEvent(object sender, EventArgs e)
        {
            CustomCheckBox c = (CustomCheckBox)sender;
            if(c == godmodecheck)
            {
                this.godmodeToolStripMenuItem.Checked = c.Checked;
            } else if(c == manacheck)
            {
                this.infManaToolStripMenuItem.Checked = c.Checked;
            } else if(c == skillcheck)
            {
                this.infSkillToolStripMenuItem.Checked = c.Checked;
            } else if(c == applecheck)
            {
                this.infApplesToolStripMenuItem.Checked = c.Checked;
            } else if(c == insanitymode)
            {
                this.insanityModeToolStripMenuItem.Checked = c.Checked;
            } else if(c == this.freezetimecheckbox)
            {
                this.freezeTimeToolStripMenuItem.Checked = c.Checked;
            }
        }

        private void menucheat(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            
        }

        private void onformclose(object sender, FormClosingEventArgs e)
        {
            this.Exit();
        }

        private void tickevent(object sender, EventArgs e)
        {
            if(this.Opacity == 1)
            {
                this.timer.Stop();
            } else
            {
                this.Opacity = this.Opacity+0.1;
            }
        }

        public void RegisterHotKeys()
        {
            this.hotkey = new HotKeyProvider(this);
            hotkey.RegisterKey(Keys.NumPad0);
            hotkey.RegisterKey(Keys.NumPad1);
            hotkey.RegisterKey(Keys.NumPad2);
            hotkey.RegisterKey(Keys.NumPad3);
            hotkey.RegisterKey(Keys.NumPad4);
            hotkey.RegisterKey(Keys.NumPad5);
            hotkey.RegisterKey(Keys.NumPad6);
            hotkey.RegisterKey(Keys.NumPad7);
            hotkey.RegisterKey(Keys.NumPad8);
            hotkey.RegisterKey(Keys.Add);
            hotkey.RegisterKey(Keys.Subtract);
            hotkey.RegisterKey(Keys.Divide);
            hotkey.RegisterKey(Keys.Down);
            hotkey.RegisterKey(Keys.Left);
            hotkey.RegisterKey(Keys.Right);
            hotkey.RegisterKey(Keys.G);
        }

        public void UnRegisterHotKeys()
        {
            HotKeyProvider hotkey = new HotKeyProvider(this);
            hotkey.UnRegisterAll();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                /* Note that the three lines below are not needed if you only want to register one hotkey.
                 * The below lines are useful in case you want to register multiple keys, which you can use a switch with the id as argument, or if you want to know which key/modifier was pressed for some particular reason. */


                int id = m.WParam.ToInt32();

                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);                  // The key of the hotkey that was pressed.

                //MessageBox.Show(key.ToString());

                if(this.m.theProc == null)
                {
                    Speech.Speak("Error!, the process was not found!");
                    return;
                }

                //use GetWindowThreadProcessId to check wether the focus is on the trainer or the game and not a completely different window.
                int processout;
                GetWindowThreadProcessId(GetForegroundWindow(), out processout);

                if (this.m.theProc.Id == processout || Process.GetCurrentProcess().Id == processout)
                {
                    if (key == Keys.NumPad0)
                    {
                        if (this.godmodecheck.Checked)
                        {
                            this.godmodecheck.Checked = false;
                            Speech.Speak("disabling godmode");
                        }
                        else
                        {
                            this.godmodecheck.Checked = true;
                            Speech.Speak("enabling godmode");
                        }
                    }
                    else if (key == Keys.NumPad1)
                    {
                        if (this.manacheck.Checked)
                        {
                            this.manacheck.Checked = false;
                            Speech.Speak("disabling infinitive mana");
                        }
                        else
                        {
                            this.manacheck.Checked = true;
                            Speech.Speak("enabling infinitive mana");
                        }
                    }
                    else if (key == Keys.NumPad2)
                    {
                        if (this.skillcheck.Checked)
                        {
                            this.skillcheck.Checked = false;
                            Speech.Speak("disabling infinitive skill");
                        }
                        else
                        {
                            this.skillcheck.Checked = true;
                            Speech.Speak("enabling infinitive skill");
                        }
                    }
                    else if (key == Keys.NumPad3)
                    {
                        if (this.insanitymode.Checked)
                        {
                            this.insanitymode.Checked = false;
                            Speech.Speak("disabling insanity mode");
                        }
                        else
                        {
                            this.insanitymode.Checked = true;
                            this.godmodecheck.Checked = false;
                            this.manacheck.Checked = false;
                            Speech.Speak("enabling insanity mode");
                        }
                    }
                    else if (key == Keys.NumPad4)
                    {
                        this.tpbtn.PerformClick();
                    }
                    else if (key == Keys.Left)
                    {
                        int currentindex = this.teleportbox.SelectedIndex;
                        if ((currentindex - 1) >= 0)
                        {
                            this.teleportbox.SelectedIndex = (currentindex - 1);
                            String name = (String)this.teleportbox.Items[this.teleportbox.SelectedIndex];
                            TeleportLocation l = TeleportRegistry.GetRegistry().ValueOf(name);
                            Speech.Speak("switching back to " + l.GetName());
                        }
                        else
                        {
                            Console.Beep(100, 800);
                        }
                    }
                    else if (key == Keys.Down)
                    {
                        this.GetTrainer().GetPlayer().TeleportUndo();
                    }
                    else if (key == Keys.Right)
                    {
                        int currentindex = this.teleportbox.SelectedIndex;
                        if ((currentindex + 1) < this.teleportbox.Items.Count)
                        {
                            this.teleportbox.SelectedIndex = (currentindex + 1);
                            String name = (String)this.teleportbox.Items[this.teleportbox.SelectedIndex];
                            TeleportLocation l = TeleportRegistry.GetRegistry().ValueOf(name);
                            Speech.Speak("switching next to " + l.GetName());
                        }
                        else
                        {
                            Console.Beep(100, 800);
                        }
                    }
                    else if (key == Keys.NumPad5)
                    {
                        if (this.applecheck.Checked)
                        {
                            this.applecheck.Checked = false;
                            Speech.Speak("disabling infinitive apples");
                        }
                        else
                        {
                            this.applecheck.Checked = true;
                            Speech.Speak("enabling infinitive apples");
                        }
                    }
                    else if (key == Keys.NumPad6)
                    {
                        this.moneybtn.PerformClick();
                    }
                    else if (key == Keys.NumPad7)
                    {
                        this.potiongivebtn.PerformClick();
                    }
                    else if (key == Keys.NumPad8)
                    {
                        this.bombbtn.PerformClick();
                    }
                    else if (key == Keys.Add)
                    {
                        this.GetTrainer().GetPlayer().SetDay();
                        Speech.Speak("setting time to day");
                    }
                    else if (key == Keys.Subtract)
                    {
                        this.GetTrainer().GetPlayer().SetNight();
                        Speech.Speak("setting time to night");
                    }
                    else if (key == Keys.Divide)
                    {
                        if (this.freezetimecheckbox.Checked)
                        {
                            this.freezetimecheckbox.Checked = false;
                            this.GetTrainer().GetPlayer().UnFreezeTime();
                            this.freezeTimeToolStripMenuItem.Checked = false;
                            Speech.Speak("unfreezing time");
                        }
                        else
                        {
                            this.freezetimecheckbox.Checked = true;
                            this.freezeTimeToolStripMenuItem.Checked = true;
                            this.GetTrainer().GetPlayer().FreezeTime();
                            Speech.Speak("freezing time");
                        }
                    }
                    else if (key == Keys.G)
                    {
                        if (this.flycheckbox.Checked)
                        {
                            this.flycheckbox.Checked = false;
                            Speech.Speak("disabling fly hack");
                        }
                        else
                        {
                            this.flycheckbox.Checked = true;
                            Speech.Speak("enabling fly hack ");
                        }
                    } else if(key == Keys.Space)
                    {
                        this.GetTrainer().GetFly().Fly();
                    }
                }
            }
        }

        public void Exit()
        {
            Application.Exit();
        }

        public void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.Location = new Point((this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.cursorholder.Cursor = this.grab;
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.cursorholder.Cursor = this.ungrab;
                this.isMouseClicked = false;
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Disablehotkeyscheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if(this.disablehotkeyscheckbox.Checked)
            {
                this.UnRegisterHotKeys();
            } else
            {
                this.RegisterHotKeys();
            }
        }

        private void Mute_CheckedChanged(object sender, EventArgs e)
        {
            if(this.mute.Checked)
            {
                this.mute.ForeColor = Color.Green;
            } else
            {
                this.mute.ForeColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            }
        }

        public Trainer GetTrainer()
        {
            return this.trainer;
        }

        private void Label6_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X+20, currentpoint.Y+80);
            this.helpwindw.ShowDialog();
        }

        private void CreditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X + 20, currentpoint.Y + 80);
            this.helpwindw.ShowDialog();
        }

        private void GithubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitlab.com/xize/borderlands2-trainer/-/releases");
        }

        private void GodmodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.godmodecheck.Checked = !this.godmodecheck.Checked;
        }

        private void InfManaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.manacheck.Checked = !this.manacheck.Checked;
        }

        private void InfSkillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.skillcheck.Checked = !this.skillcheck.Checked;
        }

        private void InfApplesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.applecheck.Checked = !this.applecheck.Checked;
        }

        private void InsanityModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.insanitymode.Checked = !this.insanitymode.Checked;
        }

        private void GiveMoneyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.moneybtn.PerformClick();
        }

        private void Moneybtn_Click(object sender, EventArgs e)
        {
            int val = Decimal.ToInt32(this.moneynumeric.Value);
            this.GetTrainer().GetPlayer().GiveMoney(val);
            Console.Beep();
        }

        private void CheckForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.OpenWebpage("https://gitlab.com/xize/cubeworldtrainer/-/releases");
        }

        public void OpenWebpage(String website)
        {
            //TODO: check if this keeps working since microsoft said this was a bug... currently I have not seen a other method what works...
            //since this program requires administrator elevation it's unable to use cmd start, or just process.start("domain...");

            Process.Start("explorer", ""+@website);
        }

        private void CreditsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X + 20, currentpoint.Y + 80);
            this.helpwindw.ShowDialog();
        }

        private async void Button1_ClickAsync(object sender, EventArgs e)
        {
            //scan aob test :P

            await Task.Run(() =>
            {
                this.Invoke((MethodInvoker)delegate
                {

                    Point currentpoint = this.Location;
                    this.trainerscan.Opacity = 0.8;
                    this.trainerscan.Show();
                    this.trainerscan.Location = new Point(currentpoint.X + 20, currentpoint.Y + 140);
                    this.trainerscan.BringToFront();
                    this.trainerscan.trainerscanprogress.Value = this.rand.Next(0, 60);

                });

                Task t = trainer.GetPlayer().LoadAOB();
                t.Wait();
                //Task t2 = trainer.GetFly().LoadAOB();
                //t2.Wait();

                this.Invoke((MethodInvoker)delegate
                {
                    this.trainerscan.trainerscanprogress.Value = 100;
                    this.trainerscan.Hide();
                });
            });
        }

        private void Potiongivebtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().GivePotions(Decimal.ToInt32(this.potionnumberic.Value));
            Console.Beep();
        }

        private void GivePotionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.potiongivebtn.PerformClick();
        }

        private void Bombbtn_Click(object sender, EventArgs e)
        {
            bool b = this.GetTrainer().GetPlayer().GiveBombs(Decimal.ToInt32(this.bombnumeric.Value));
            if(b)
            {
                Console.Beep();
            }
        }

        private void GiveBombsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.bombbtn.PerformClick();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.teleportwindw.Show();

            Point loc = this.Location;

            loc.Offset(450, 100);

            this.teleportwindw.Location = loc;
        }

        public void RebuildTeleportList()
        {
            this.teleportbox.Items.Clear();
            foreach (TeleportLocation l in TeleportRegistry.GetRegistry().Values())
            {
                this.teleportbox.Items.Add(l.GetName());
            }
            if(this.teleportbox.Items.Count > 0)
            {
                this.teleportbox.SelectedIndex = 0;
            }
            
        }

        private void Tpbtn_Click(object sender, EventArgs e)
        {
            String name = (String)this.teleportbox.SelectedItem;

            if(name != null)
            {
                TeleportLocation loc = TeleportRegistry.GetRegistry().ValueOf(name);
                if(loc != null)
                {
                    this.GetTrainer().GetPlayer().TeleportTo(loc.GetX(), loc.GetY(), loc.GetZ());
                    Speech.Speak("teleporting to "+loc.GetName());
                } else
                {
                    this.RebuildTeleportList();
                }
            } else
            {
                this.RebuildTeleportList();
            }

        }

        private void Daybtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetDay();
        }

        private void Nightbtn_Click(object sender, EventArgs e)
        {
            this.GetTrainer().GetPlayer().SetNight();
        }

        private void SetDayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.daybtn.PerformClick();
        }

        private void SetNightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.nightbtn.PerformClick();
        }

        private void DoFlyCheckBoxEvent(object sender, EventArgs e)
        {
            if(this.flycheckbox.Checked)
            {
                this.hotkey.RegisterKey(Keys.Space);
            } else
            {
                this.UnRegisterHotKeys();
                this.RegisterHotKeys();
            }
        }

        private void FreezeTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.freezetimecheckbox.Checked)
            {
                this.freezetimecheckbox.Checked = false;
                this.GetTrainer().GetPlayer().UnFreezeTime();
            }
            else
            {
                this.freezetimecheckbox.Checked = true;
                this.GetTrainer().GetPlayer().FreezeTime();
            }
        }
    }
}

﻿namespace CubeWorldTrainer
{
    partial class LoadWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadWindow));
            this.version = new System.Windows.Forms.Label();
            this.debuglabel = new System.Windows.Forms.Label();
            this.loadprogress = new System.Windows.Forms.Label();
            this.loadingani = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.loadingani)).BeginInit();
            this.SuspendLayout();
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.Transparent;
            this.version.ForeColor = System.Drawing.Color.White;
            this.version.Location = new System.Drawing.Point(380, 204);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(84, 13);
            this.version.TabIndex = 1;
            this.version.Text = "trainer ver: {0}.A";
            // 
            // debuglabel
            // 
            this.debuglabel.AutoSize = true;
            this.debuglabel.BackColor = System.Drawing.Color.Transparent;
            this.debuglabel.ForeColor = System.Drawing.Color.White;
            this.debuglabel.Location = new System.Drawing.Point(379, 191);
            this.debuglabel.Name = "debuglabel";
            this.debuglabel.Size = new System.Drawing.Size(61, 13);
            this.debuglabel.TabIndex = 2;
            this.debuglabel.Text = "Debug: yes";
            this.debuglabel.Visible = false;
            // 
            // loadprogress
            // 
            this.loadprogress.AutoSize = true;
            this.loadprogress.BackColor = System.Drawing.Color.Transparent;
            this.loadprogress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadprogress.ForeColor = System.Drawing.Color.LightBlue;
            this.loadprogress.Location = new System.Drawing.Point(81, 201);
            this.loadprogress.Name = "loadprogress";
            this.loadprogress.Size = new System.Drawing.Size(42, 17);
            this.loadprogress.TabIndex = 3;
            this.loadprogress.Text = "{0}%";
            // 
            // loadingani
            // 
            this.loadingani.BackColor = System.Drawing.Color.Transparent;
            this.loadingani.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.ezgif_4_2953749d2ff8;
            this.loadingani.Location = new System.Drawing.Point(35, 187);
            this.loadingani.Name = "loadingani";
            this.loadingani.Size = new System.Drawing.Size(40, 40);
            this.loadingani.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loadingani.TabIndex = 4;
            this.loadingani.TabStop = false;
            // 
            // LoadWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.loadingscr2_new2;
            this.ClientSize = new System.Drawing.Size(500, 234);
            this.Controls.Add(this.loadingani);
            this.Controls.Add(this.loadprogress);
            this.Controls.Add(this.debuglabel);
            this.Controls.Add(this.version);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadWindow";
            this.Load += new System.EventHandler(this.LoadWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loadingani)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Label debuglabel;
        private System.Windows.Forms.Label loadprogress;
        private System.Windows.Forms.PictureBox loadingani;
    }
}
﻿using CubeWorldTrainer.cheats;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    public partial class TeleportWindow : Form
    {

        private bool isMouseClicked = false;
        private Point lastLocation;
        private Cursor ungrab;
        private Cursor grab;
        private Window win;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public TeleportWindow(Window win)
        {
            this.win = win;
            this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());
            InitializeComponent();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }


        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.Location = new Point((this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.grab;
                this.isMouseClicked = true;
                this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Cursor = this.ungrab;
                this.isMouseClicked = false;
            }
        }

        private void TeleportWindow_Load(object sender, EventArgs e)
        {
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            this.Cursor = this.ungrab;
            this.makeMoveAble(this);

            this.teleportlist.EditChanged += new EventHandler(RenamedLocation);

            TeleportRegistry.GetRegistry().LoadFromFile();
            TeleportLocation[] locs = TeleportRegistry.GetRegistry().Values();
            foreach(TeleportLocation l in locs)
            {
                this.teleportlist.Items.Add(l.GetName());
            }

        }

        private void RenamedLocation(object sender, EventArgs e)
        {
            String[] data = ((String)sender).Split(',');
            String oldstring = data[0];
            String newstring = data[1];

            if(newstring != oldstring)
            {
                TeleportRegistry.GetRegistry().RenameLocation(newstring, oldstring);
            }

        }

        private void Resetbtn_Click(object sender, EventArgs e)
        {
            this.teleporttextbox.Text = "";
        }

        private void Addbtn_Click(object sender, EventArgs e)
        {
            IPlayer p = this.win.GetTrainer().GetPlayer();
            String name = this.teleporttextbox.Text;
            float x = p.GetX();
            float y = p.GetY();
            float z = p.GetZ();

            TeleportLocation tp = new TeleportLocation(name, x, y, z);

            TeleportRegistry.GetRegistry().Add(tp);
            if(!this.teleportlist.Items.Contains(tp.GetName()))
            {
                this.teleportlist.Items.Add(tp.GetName());
            }
        }

        private void Delbtn_Click(object sender, EventArgs e)
        {
            
            int selindex = this.teleportlist.SelectedIndex;

            if(selindex == -1)
            {
                return;
            }

            String name = this.teleportlist.Text;

            this.teleportlist.Items.RemoveAt(selindex);

            TeleportLocation tp = TeleportRegistry.GetRegistry().ValueOf(name);
            TeleportRegistry.GetRegistry().Remove(tp);

        }

        private void Savebtn_Click(object sender, EventArgs e)
        {
            TeleportRegistry.GetRegistry().SaveToFile();
        }

        private void Loadbtn_Click(object sender, EventArgs e)
        {
            TeleportRegistry.GetRegistry().LoadFromFile();
            TeleportLocation[] locs = TeleportRegistry.GetRegistry().Values();
            foreach (TeleportLocation l in locs)
            {
                if (!this.teleportlist.Items.Contains(l.GetName()))
                {
                    this.teleportlist.Items.Add(l.GetName());
                }
            }
        }

        private void Backupbtn_Click(object sender, EventArgs e)
        {
            String path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\0c3\cubeworldtrainer";
            Process.Start("Explorer.exe", path);
        }

        private void Syncbtn_Click(object sender, EventArgs e)
        {
            this.win.RebuildTeleportList();
        }
    }
}

﻿using CubeWorldTrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer.util
{
    class Speech
    {

        private static SpeechSynthesizer speech;

        public static void Speak(CheckBox check, String message)
        {
            if (speech == null)
            {
                speech = new SpeechSynthesizer();
            }
            if (!check.Checked)
            {
                speech.SpeakAsyncCancelAll();
                speech.SpeakAsync(message);
            }
        }

        public static void Speak(String message)
        {
            if (speech == null)
            {
                speech = new SpeechSynthesizer();
            }
            speech.SpeakAsyncCancelAll();
            speech.SpeakAsync(message);
        }

    }
}

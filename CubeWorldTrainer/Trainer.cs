﻿
using CubeWorldTrainer.cheats;
using CubeWorldTrainer.util;
using Memory;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    public class Trainer
    {

        private Mem m;
        private Window win;
        private bool hooked = false;
        private BackgroundWorker worker = new BackgroundWorker();


        //cheats

        private IPlayer player;
        private IFly fly;

        //end cheats

        public Trainer(Window win, Mem m)
        {
            this.win = win;
            this.m = m;

            //instance the cheats.
            this.InitializeCheats(win, m);
        }

        public void InitializeCheats(Form win, Mem m)
        {

            //TEST
            this.player = new PlayerModel(this.m);
            this.fly = new FlyModel(this.m);
            //END TEST :)
        }

        public IPlayer GetPlayer()
        {
            return this.player;
        }

        public IFly GetFly()
        {
            return this.fly;
        }

        public bool OpenGame()
        {

            int PID = this.m.getProcIDFromName("cubeworld.exe");

            if (this.hooked && PID > 0)
                return true;

            if (PID > 0)
            {
                if(!this.hooked)
                {
                    bool bol = this.m.OpenProcess(PID);
                    if(bol)
                    {
                        this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.ForeColor = Color.Green));
                        this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.Text = "success"));
                        this.hooked = true;
                        Console.Beep(500, 100);
                        Console.Beep(200, 100);
                        Console.Beep(500, 100);

                        return true;
                    }
                }
            } else {
                    this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.ForeColor = Color.Red));
                    this.win.hookedstatus.Invoke(new Action(() => this.win.hookedstatus.Text = "false"));
                    MessageBox.Show("start cubeworld first or the trainer will not work !");
                    this.win.Invoke(new Action(() => this.win.Exit()));
                    this.hooked = false;
    }
            return false;
        }


        public void Start()
        {
            if(!worker.IsBusy)
            {
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnBackgroundWorkerCompletedEvent);
                worker.DoWork += new DoWorkEventHandler(OnDoWorkEvent);
                worker.RunWorkerAsync();
            }
        }

        private void OnDoWorkEvent(Object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                this.OpenGame();

                if (this.GetPlayer().IsAOB())
                {
                    this.win.aobresult.Invoke(new Action(() => this.win.aobresult.ForeColor = Color.Green));
                    this.win.aobresult.Invoke(new Action(() => this.win.aobresult.Text = ((PlayerModel)this.GetPlayer()).GetBaseAddress()));
                }

                if (!this.hooked)
                    return;

                if(this.win.godmodecheck.Checked)
                {
                    this.player.DoGodmode();
                }
                
                if(this.win.manacheck.Checked)
                {
                    this.player.DoInfMana();
                }

                if(this.win.skillcheck.Checked)
                {
                    this.player.DoInfSkill();
                }

                if(this.win.applecheck.Checked)
                {
                    this.player.DoInfApples();
                }

                if(this.win.insanitymode.Checked)
                {
                    this.player.DoCrazyHealth();
                    this.player.DoCrazyMana();
                }
            }
        }

        private void OnBackgroundWorkerCompletedEvent(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error is Exception)
            {


                int currentval = Int32.Parse(this.win.errorstatus.Text);

                this.win.errorstatus.Text = ""+(currentval + 1);
                this.win.errorstatus.ForeColor = Color.Red;

                //use this for debugging the game.

                this.Start();
            }
        }

    }
}

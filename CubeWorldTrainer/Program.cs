﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length == 0)
            {
                Application.Run(new LoadWindow());
            } else if(args.Length == 1) {
                if(args[0].ToLower() == "-debug")
                {
                    Application.Run(new LoadWindow(true));
                } else
                {
                    MessageBox.Show("invalid argument!");
                }
            } else
            {
                MessageBox.Show("invalid argument!");
            }
        }

    }
}

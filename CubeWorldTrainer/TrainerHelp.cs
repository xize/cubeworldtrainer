﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    public partial class TrainerHelp : Form
    {

        public TrainerHelp()
        {
            InitializeComponent();
            this.label4.Text = String.Format(label4.Text, Application.ProductVersion);
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}

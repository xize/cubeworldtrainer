﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeWorldTrainer
{
    class TeleportLocation
    {

        private String name;
        private float x;
        private float y;
        private float z;

        public TeleportLocation(String name, float x, float y, float z)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public String GetName()
        {
            return this.name;
        }

        public float GetX()
        {
            return this.x;
        }

        public float GetY()
        {
            return this.y;
        }

        public float GetZ()
        {
            return this.z;
        }

        public bool Equals(TeleportLocation l)
        {
            if(this.name.Equals(l.GetName()) && this.x == l.GetX() && this.y == l.GetY() && this.z == l.GetZ() && this.GetHashCode() == l.GetHashCode())
            {
                return true;
            }

            return false;
        }

        
        public override int GetHashCode()
        {
            int seed = 3912;
            int hash = seed * this.GetName().GetHashCode() - 8391102 + this.GetX().GetHashCode() + this.GetY().GetHashCode() + this.GetZ().GetHashCode();
            return hash;
        }

    }
}

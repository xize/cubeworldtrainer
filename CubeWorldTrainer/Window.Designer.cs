﻿namespace CubeWorldTrainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.aobresult = new System.Windows.Forms.Label();
            this.aoblabel = new System.Windows.Forms.Label();
            this.errorstatus = new System.Windows.Forms.Label();
            this.error = new System.Windows.Forms.Label();
            this.giveMoneyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerpanel = new System.Windows.Forms.Panel();
            this.freezetimelabel = new System.Windows.Forms.Label();
            this.nightbtn = new System.Windows.Forms.Button();
            this.nightlabel = new System.Windows.Forms.Label();
            this.daybtn = new System.Windows.Forms.Button();
            this.daytimelabel = new System.Windows.Forms.Label();
            this.tpbtn = new System.Windows.Forms.Button();
            this.teleporteditbtn = new System.Windows.Forms.Button();
            this.teleportbox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.bombbtn = new System.Windows.Forms.Button();
            this.bombnumeric = new System.Windows.Forms.NumericUpDown();
            this.bomblabel = new System.Windows.Forms.Label();
            this.potiongivebtn = new System.Windows.Forms.Button();
            this.potionnumberic = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.moneybtn = new System.Windows.Forms.Button();
            this.mute = new System.Windows.Forms.CheckBox();
            this.disablehotkeyscheckbox = new System.Windows.Forms.CheckBox();
            this.moneynumeric = new System.Windows.Forms.NumericUpDown();
            this.moneylabel = new System.Windows.Forms.Label();
            this.hookedstatus = new System.Windows.Forms.Label();
            this.infappleslabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.infintivegrenadeslabel = new System.Windows.Forms.Label();
            this.infinitiveshieldlabel = new System.Windows.Forms.Label();
            this.godmodelabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.playerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.godmodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infManaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infSkillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insanityModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infApplesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giveMoneyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.givePotionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giveBombsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cursorholder = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.setDayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setNightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.freezeTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flylabel = new System.Windows.Forms.Label();
            this.flycheckbox = new CubeWorldTrainer.controls.CustomCheckBox();
            this.freezetimecheckbox = new CubeWorldTrainer.controls.CustomCheckBox();
            this.insanitymode = new CubeWorldTrainer.controls.CustomCheckBox();
            this.applecheck = new CubeWorldTrainer.controls.CustomCheckBox();
            this.skillcheck = new CubeWorldTrainer.controls.CustomCheckBox();
            this.manacheck = new CubeWorldTrainer.controls.CustomCheckBox();
            this.godmodecheck = new CubeWorldTrainer.controls.CustomCheckBox();
            this.panel2.SuspendLayout();
            this.centerpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bombnumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.potionnumberic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumeric)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.cursorholder.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(237, 392);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 106;
            this.label12.Text = "What is this?";
            this.tooltip.SetToolTip(this.label12, "when the trainer fails, or the game is updated\r\nthis can still search the correct" +
        " memory base address.\r\nby using a unique signature of bytes :)");
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(353, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "credits";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.version.Dock = System.Windows.Forms.DockStyle.Left;
            this.version.ForeColor = System.Drawing.Color.White;
            this.version.Location = new System.Drawing.Point(0, 0);
            this.version.Name = "version";
            this.version.Padding = new System.Windows.Forms.Padding(5);
            this.version.Size = new System.Drawing.Size(59, 23);
            this.version.TabIndex = 45;
            this.version.Text = "ver {0}.A";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.aobresult);
            this.panel2.Controls.Add(this.aoblabel);
            this.panel2.Controls.Add(this.errorstatus);
            this.panel2.Controls.Add(this.error);
            this.panel2.Controls.Add(this.version);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 472);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 23);
            this.panel2.TabIndex = 46;
            // 
            // aobresult
            // 
            this.aobresult.AutoSize = true;
            this.aobresult.Dock = System.Windows.Forms.DockStyle.Left;
            this.aobresult.ForeColor = System.Drawing.Color.Red;
            this.aobresult.Location = new System.Drawing.Point(155, 0);
            this.aobresult.Name = "aobresult";
            this.aobresult.Padding = new System.Windows.Forms.Padding(5, 5, 1, 5);
            this.aobresult.Size = new System.Drawing.Size(60, 23);
            this.aobresult.TabIndex = 49;
            this.aobresult.Text = "not active";
            // 
            // aoblabel
            // 
            this.aoblabel.AutoSize = true;
            this.aoblabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.aoblabel.ForeColor = System.Drawing.Color.White;
            this.aoblabel.Location = new System.Drawing.Point(121, 0);
            this.aoblabel.Name = "aoblabel";
            this.aoblabel.Padding = new System.Windows.Forms.Padding(5, 5, 1, 5);
            this.aoblabel.Size = new System.Drawing.Size(34, 23);
            this.aoblabel.TabIndex = 48;
            this.aoblabel.Text = "aob:";
            // 
            // errorstatus
            // 
            this.errorstatus.AutoSize = true;
            this.errorstatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.errorstatus.ForeColor = System.Drawing.Color.White;
            this.errorstatus.Location = new System.Drawing.Point(102, 0);
            this.errorstatus.Name = "errorstatus";
            this.errorstatus.Padding = new System.Windows.Forms.Padding(1, 5, 5, 5);
            this.errorstatus.Size = new System.Drawing.Size(19, 23);
            this.errorstatus.TabIndex = 47;
            this.errorstatus.Text = "0";
            // 
            // error
            // 
            this.error.AutoSize = true;
            this.error.Dock = System.Windows.Forms.DockStyle.Left;
            this.error.ForeColor = System.Drawing.Color.White;
            this.error.Location = new System.Drawing.Point(59, 0);
            this.error.Name = "error";
            this.error.Padding = new System.Windows.Forms.Padding(5, 5, 1, 5);
            this.error.Size = new System.Drawing.Size(43, 23);
            this.error.TabIndex = 46;
            this.error.Text = "Errors:";
            // 
            // giveMoneyToolStripMenuItem
            // 
            this.giveMoneyToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.giveMoneyToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.giveMoneyToolStripMenuItem.Name = "giveMoneyToolStripMenuItem";
            this.giveMoneyToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.giveMoneyToolStripMenuItem.Text = "give money";
            // 
            // centerpanel
            // 
            this.centerpanel.BackColor = System.Drawing.Color.Black;
            this.centerpanel.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.center2;
            this.centerpanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.centerpanel.Controls.Add(this.flycheckbox);
            this.centerpanel.Controls.Add(this.flylabel);
            this.centerpanel.Controls.Add(this.freezetimecheckbox);
            this.centerpanel.Controls.Add(this.freezetimelabel);
            this.centerpanel.Controls.Add(this.nightbtn);
            this.centerpanel.Controls.Add(this.nightlabel);
            this.centerpanel.Controls.Add(this.daybtn);
            this.centerpanel.Controls.Add(this.daytimelabel);
            this.centerpanel.Controls.Add(this.tpbtn);
            this.centerpanel.Controls.Add(this.teleporteditbtn);
            this.centerpanel.Controls.Add(this.teleportbox);
            this.centerpanel.Controls.Add(this.label13);
            this.centerpanel.Controls.Add(this.label12);
            this.centerpanel.Controls.Add(this.bombbtn);
            this.centerpanel.Controls.Add(this.bombnumeric);
            this.centerpanel.Controls.Add(this.bomblabel);
            this.centerpanel.Controls.Add(this.potiongivebtn);
            this.centerpanel.Controls.Add(this.potionnumberic);
            this.centerpanel.Controls.Add(this.label7);
            this.centerpanel.Controls.Add(this.button1);
            this.centerpanel.Controls.Add(this.insanitymode);
            this.centerpanel.Controls.Add(this.label5);
            this.centerpanel.Controls.Add(this.applecheck);
            this.centerpanel.Controls.Add(this.skillcheck);
            this.centerpanel.Controls.Add(this.manacheck);
            this.centerpanel.Controls.Add(this.godmodecheck);
            this.centerpanel.Controls.Add(this.moneybtn);
            this.centerpanel.Controls.Add(this.mute);
            this.centerpanel.Controls.Add(this.disablehotkeyscheckbox);
            this.centerpanel.Controls.Add(this.moneynumeric);
            this.centerpanel.Controls.Add(this.moneylabel);
            this.centerpanel.Controls.Add(this.hookedstatus);
            this.centerpanel.Controls.Add(this.infappleslabel);
            this.centerpanel.Controls.Add(this.label10);
            this.centerpanel.Controls.Add(this.label8);
            this.centerpanel.Controls.Add(this.label9);
            this.centerpanel.Controls.Add(this.infintivegrenadeslabel);
            this.centerpanel.Controls.Add(this.infinitiveshieldlabel);
            this.centerpanel.Controls.Add(this.godmodelabel);
            this.centerpanel.Controls.Add(this.label4);
            this.centerpanel.Controls.Add(this.label3);
            this.centerpanel.Controls.Add(this.panel1);
            this.centerpanel.Controls.Add(this.menuStrip1);
            this.centerpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.centerpanel.Location = new System.Drawing.Point(0, 52);
            this.centerpanel.Name = "centerpanel";
            this.centerpanel.Size = new System.Drawing.Size(400, 414);
            this.centerpanel.TabIndex = 50;
            // 
            // freezetimelabel
            // 
            this.freezetimelabel.AutoSize = true;
            this.freezetimelabel.BackColor = System.Drawing.Color.Transparent;
            this.freezetimelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.freezetimelabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.freezetimelabel.Location = new System.Drawing.Point(131, 340);
            this.freezetimelabel.Name = "freezetimelabel";
            this.freezetimelabel.Size = new System.Drawing.Size(98, 13);
            this.freezetimelabel.TabIndex = 115;
            this.freezetimelabel.Text = "num / : freeze time ";
            // 
            // nightbtn
            // 
            this.nightbtn.AutoSize = true;
            this.nightbtn.BackColor = System.Drawing.Color.Black;
            this.nightbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nightbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.nightbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nightbtn.ForeColor = System.Drawing.Color.White;
            this.nightbtn.Location = new System.Drawing.Point(335, 307);
            this.nightbtn.Name = "nightbtn";
            this.nightbtn.Size = new System.Drawing.Size(44, 25);
            this.nightbtn.TabIndex = 114;
            this.nightbtn.Text = "night";
            this.nightbtn.UseVisualStyleBackColor = false;
            this.nightbtn.Click += new System.EventHandler(this.Nightbtn_Click);
            // 
            // nightlabel
            // 
            this.nightlabel.AutoSize = true;
            this.nightlabel.BackColor = System.Drawing.Color.Transparent;
            this.nightlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nightlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.nightlabel.Location = new System.Drawing.Point(131, 311);
            this.nightlabel.Name = "nightlabel";
            this.nightlabel.Size = new System.Drawing.Size(65, 13);
            this.nightlabel.TabIndex = 113;
            this.nightlabel.Text = "num - : night";
            // 
            // daybtn
            // 
            this.daybtn.AutoSize = true;
            this.daybtn.BackColor = System.Drawing.Color.Black;
            this.daybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.daybtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.daybtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.daybtn.ForeColor = System.Drawing.Color.White;
            this.daybtn.Location = new System.Drawing.Point(336, 278);
            this.daybtn.Name = "daybtn";
            this.daybtn.Size = new System.Drawing.Size(44, 25);
            this.daybtn.TabIndex = 112;
            this.daybtn.Text = "day";
            this.daybtn.UseVisualStyleBackColor = false;
            this.daybtn.Click += new System.EventHandler(this.Daybtn_Click);
            // 
            // daytimelabel
            // 
            this.daytimelabel.AutoSize = true;
            this.daytimelabel.BackColor = System.Drawing.Color.Transparent;
            this.daytimelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daytimelabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.daytimelabel.Location = new System.Drawing.Point(132, 282);
            this.daytimelabel.Name = "daytimelabel";
            this.daytimelabel.Size = new System.Drawing.Size(62, 13);
            this.daytimelabel.TabIndex = 111;
            this.daytimelabel.Text = "num + : day";
            // 
            // tpbtn
            // 
            this.tpbtn.AutoSize = true;
            this.tpbtn.BackColor = System.Drawing.Color.Black;
            this.tpbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tpbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.tpbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tpbtn.ForeColor = System.Drawing.Color.White;
            this.tpbtn.Location = new System.Drawing.Point(335, 143);
            this.tpbtn.Name = "tpbtn";
            this.tpbtn.Size = new System.Drawing.Size(44, 25);
            this.tpbtn.TabIndex = 110;
            this.tpbtn.Text = "TP";
            this.tpbtn.UseVisualStyleBackColor = false;
            this.tpbtn.Click += new System.EventHandler(this.Tpbtn_Click);
            // 
            // teleporteditbtn
            // 
            this.teleporteditbtn.AutoSize = true;
            this.teleporteditbtn.BackColor = System.Drawing.Color.Black;
            this.teleporteditbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.teleporteditbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.teleporteditbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teleporteditbtn.ForeColor = System.Drawing.Color.White;
            this.teleporteditbtn.Location = new System.Drawing.Point(286, 143);
            this.teleporteditbtn.Name = "teleporteditbtn";
            this.teleporteditbtn.Size = new System.Drawing.Size(44, 25);
            this.teleporteditbtn.TabIndex = 109;
            this.teleporteditbtn.Text = "Edit";
            this.teleporteditbtn.UseVisualStyleBackColor = false;
            this.teleporteditbtn.Click += new System.EventHandler(this.Button2_Click);
            // 
            // teleportbox
            // 
            this.teleportbox.BackColor = System.Drawing.Color.Black;
            this.teleportbox.DropDownWidth = 400;
            this.teleportbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teleportbox.ForeColor = System.Drawing.Color.White;
            this.teleportbox.FormattingEnabled = true;
            this.teleportbox.Location = new System.Drawing.Point(215, 146);
            this.teleportbox.MaxLength = 100;
            this.teleportbox.Name = "teleportbox";
            this.teleportbox.Size = new System.Drawing.Size(65, 21);
            this.teleportbox.TabIndex = 108;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label13.Location = new System.Drawing.Point(132, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 107;
            this.label13.Text = "num 4: teleport:";
            // 
            // bombbtn
            // 
            this.bombbtn.AutoSize = true;
            this.bombbtn.BackColor = System.Drawing.Color.Black;
            this.bombbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bombbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.bombbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bombbtn.ForeColor = System.Drawing.Color.White;
            this.bombbtn.Location = new System.Drawing.Point(336, 249);
            this.bombbtn.Name = "bombbtn";
            this.bombbtn.Size = new System.Drawing.Size(44, 25);
            this.bombbtn.TabIndex = 105;
            this.bombbtn.Text = "give";
            this.bombbtn.UseVisualStyleBackColor = false;
            this.bombbtn.Click += new System.EventHandler(this.Bombbtn_Click);
            // 
            // bombnumeric
            // 
            this.bombnumeric.BackColor = System.Drawing.Color.Black;
            this.bombnumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bombnumeric.ForeColor = System.Drawing.Color.White;
            this.bombnumeric.Location = new System.Drawing.Point(273, 252);
            this.bombnumeric.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.bombnumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bombnumeric.Name = "bombnumeric";
            this.bombnumeric.Size = new System.Drawing.Size(57, 20);
            this.bombnumeric.TabIndex = 104;
            this.bombnumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bomblabel
            // 
            this.bomblabel.AutoSize = true;
            this.bomblabel.BackColor = System.Drawing.Color.Transparent;
            this.bomblabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bomblabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.bomblabel.Location = new System.Drawing.Point(132, 253);
            this.bomblabel.Name = "bomblabel";
            this.bomblabel.Size = new System.Drawing.Size(99, 13);
            this.bomblabel.TabIndex = 103;
            this.bomblabel.Text = "num 8: give bombs:";
            // 
            // potiongivebtn
            // 
            this.potiongivebtn.AutoSize = true;
            this.potiongivebtn.BackColor = System.Drawing.Color.Black;
            this.potiongivebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.potiongivebtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.potiongivebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.potiongivebtn.ForeColor = System.Drawing.Color.White;
            this.potiongivebtn.Location = new System.Drawing.Point(336, 222);
            this.potiongivebtn.Name = "potiongivebtn";
            this.potiongivebtn.Size = new System.Drawing.Size(44, 25);
            this.potiongivebtn.TabIndex = 102;
            this.potiongivebtn.Text = "give";
            this.potiongivebtn.UseVisualStyleBackColor = false;
            this.potiongivebtn.Click += new System.EventHandler(this.Potiongivebtn_Click);
            // 
            // potionnumberic
            // 
            this.potionnumberic.BackColor = System.Drawing.Color.Black;
            this.potionnumberic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.potionnumberic.ForeColor = System.Drawing.Color.White;
            this.potionnumberic.Location = new System.Drawing.Point(273, 225);
            this.potionnumberic.Maximum = new decimal(new int[] {
            800000000,
            0,
            0,
            0});
            this.potionnumberic.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.potionnumberic.Name = "potionnumberic";
            this.potionnumberic.Size = new System.Drawing.Size(57, 20);
            this.potionnumberic.TabIndex = 101;
            this.potionnumberic.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label7.Location = new System.Drawing.Point(132, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 100;
            this.label7.Text = "num 7: give potions:";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(311, 386);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 25);
            this.button1.TabIndex = 99;
            this.button1.Text = "Scan AOB";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_ClickAsync);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label5.Location = new System.Drawing.Point(132, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 97;
            this.label5.Text = "num 3: insanity mode";
            // 
            // moneybtn
            // 
            this.moneybtn.AutoSize = true;
            this.moneybtn.BackColor = System.Drawing.Color.Black;
            this.moneybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moneybtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.moneybtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moneybtn.ForeColor = System.Drawing.Color.White;
            this.moneybtn.Location = new System.Drawing.Point(336, 195);
            this.moneybtn.Name = "moneybtn";
            this.moneybtn.Size = new System.Drawing.Size(44, 25);
            this.moneybtn.TabIndex = 92;
            this.moneybtn.Text = "give";
            this.moneybtn.UseVisualStyleBackColor = false;
            this.moneybtn.Click += new System.EventHandler(this.Moneybtn_Click);
            // 
            // mute
            // 
            this.mute.AutoSize = true;
            this.mute.BackColor = System.Drawing.Color.Transparent;
            this.mute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.mute.Location = new System.Drawing.Point(14, 232);
            this.mute.Name = "mute";
            this.mute.Size = new System.Drawing.Size(49, 17);
            this.mute.TabIndex = 80;
            this.mute.Text = "mute";
            this.mute.UseVisualStyleBackColor = false;
            // 
            // disablehotkeyscheckbox
            // 
            this.disablehotkeyscheckbox.AutoSize = true;
            this.disablehotkeyscheckbox.BackColor = System.Drawing.Color.Transparent;
            this.disablehotkeyscheckbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.disablehotkeyscheckbox.Location = new System.Drawing.Point(14, 208);
            this.disablehotkeyscheckbox.Name = "disablehotkeyscheckbox";
            this.disablehotkeyscheckbox.Size = new System.Drawing.Size(105, 17);
            this.disablehotkeyscheckbox.TabIndex = 78;
            this.disablehotkeyscheckbox.Text = "disable hotkeys?";
            this.disablehotkeyscheckbox.UseVisualStyleBackColor = false;
            // 
            // moneynumeric
            // 
            this.moneynumeric.BackColor = System.Drawing.Color.Black;
            this.moneynumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.moneynumeric.ForeColor = System.Drawing.Color.White;
            this.moneynumeric.Location = new System.Drawing.Point(273, 198);
            this.moneynumeric.Maximum = new decimal(new int[] {
            800000000,
            0,
            0,
            0});
            this.moneynumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.moneynumeric.Name = "moneynumeric";
            this.moneynumeric.Size = new System.Drawing.Size(57, 20);
            this.moneynumeric.TabIndex = 65;
            this.moneynumeric.Tag = "$";
            this.moneynumeric.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // moneylabel
            // 
            this.moneylabel.AutoSize = true;
            this.moneylabel.BackColor = System.Drawing.Color.Transparent;
            this.moneylabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moneylabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.moneylabel.Location = new System.Drawing.Point(132, 199);
            this.moneylabel.Name = "moneylabel";
            this.moneylabel.Size = new System.Drawing.Size(102, 13);
            this.moneylabel.TabIndex = 64;
            this.moneylabel.Text = "num 6: give money: ";
            // 
            // hookedstatus
            // 
            this.hookedstatus.AutoSize = true;
            this.hookedstatus.BackColor = System.Drawing.Color.Transparent;
            this.hookedstatus.ForeColor = System.Drawing.Color.Red;
            this.hookedstatus.Location = new System.Drawing.Point(55, 187);
            this.hookedstatus.Name = "hookedstatus";
            this.hookedstatus.Size = new System.Drawing.Size(29, 13);
            this.hookedstatus.TabIndex = 60;
            this.hookedstatus.Text = "false";
            // 
            // infappleslabel
            // 
            this.infappleslabel.AutoSize = true;
            this.infappleslabel.BackColor = System.Drawing.Color.Transparent;
            this.infappleslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infappleslabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.infappleslabel.Location = new System.Drawing.Point(132, 176);
            this.infappleslabel.Name = "infappleslabel";
            this.infappleslabel.Size = new System.Drawing.Size(87, 13);
            this.infappleslabel.TabIndex = 59;
            this.infappleslabel.Text = "num 5: inf apples";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label10.Location = new System.Drawing.Point(11, 187);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Hooked:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(12, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "cubeworld.exe";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label9.Location = new System.Drawing.Point(12, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "target process:";
            // 
            // infintivegrenadeslabel
            // 
            this.infintivegrenadeslabel.AutoSize = true;
            this.infintivegrenadeslabel.BackColor = System.Drawing.Color.Transparent;
            this.infintivegrenadeslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infintivegrenadeslabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.infintivegrenadeslabel.Location = new System.Drawing.Point(132, 100);
            this.infintivegrenadeslabel.Name = "infintivegrenadeslabel";
            this.infintivegrenadeslabel.Size = new System.Drawing.Size(73, 13);
            this.infintivegrenadeslabel.TabIndex = 55;
            this.infintivegrenadeslabel.Text = "num 2: inf skill";
            // 
            // infinitiveshieldlabel
            // 
            this.infinitiveshieldlabel.AutoSize = true;
            this.infinitiveshieldlabel.BackColor = System.Drawing.Color.Transparent;
            this.infinitiveshieldlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infinitiveshieldlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.infinitiveshieldlabel.Location = new System.Drawing.Point(132, 79);
            this.infinitiveshieldlabel.Name = "infinitiveshieldlabel";
            this.infinitiveshieldlabel.Size = new System.Drawing.Size(82, 13);
            this.infinitiveshieldlabel.TabIndex = 54;
            this.infinitiveshieldlabel.Text = "num 1: inf mana";
            // 
            // godmodelabel
            // 
            this.godmodelabel.AutoSize = true;
            this.godmodelabel.BackColor = System.Drawing.Color.Transparent;
            this.godmodelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.godmodelabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.godmodelabel.Location = new System.Drawing.Point(132, 59);
            this.godmodelabel.Name = "godmodelabel";
            this.godmodelabel.Size = new System.Drawing.Size(86, 13);
            this.godmodelabel.TabIndex = 53;
            this.godmodelabel.Text = "num 0: godmode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(131, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 52;
            this.label4.Text = "Cheats:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(15, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Game:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.cube_world_logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(15, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(107, 94);
            this.panel1.TabIndex = 50;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playerToolStripMenuItem,
            this.worldToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(400, 24);
            this.menuStrip1.TabIndex = 88;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // playerToolStripMenuItem
            // 
            this.playerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.godmodeToolStripMenuItem,
            this.infManaToolStripMenuItem,
            this.infSkillToolStripMenuItem,
            this.insanityModeToolStripMenuItem,
            this.infApplesToolStripMenuItem,
            this.giveMoneyToolStripMenuItem1,
            this.givePotionsToolStripMenuItem,
            this.giveBombsToolStripMenuItem});
            this.playerToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.playerToolStripMenuItem.Name = "playerToolStripMenuItem";
            this.playerToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.playerToolStripMenuItem.Text = "&Player";
            // 
            // godmodeToolStripMenuItem
            // 
            this.godmodeToolStripMenuItem.CheckOnClick = true;
            this.godmodeToolStripMenuItem.Name = "godmodeToolStripMenuItem";
            this.godmodeToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.godmodeToolStripMenuItem.Text = "godmode";
            this.godmodeToolStripMenuItem.Click += new System.EventHandler(this.GodmodeToolStripMenuItem_Click);
            // 
            // infManaToolStripMenuItem
            // 
            this.infManaToolStripMenuItem.CheckOnClick = true;
            this.infManaToolStripMenuItem.Name = "infManaToolStripMenuItem";
            this.infManaToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.infManaToolStripMenuItem.Text = "inf mana";
            this.infManaToolStripMenuItem.Click += new System.EventHandler(this.InfManaToolStripMenuItem_Click);
            // 
            // infSkillToolStripMenuItem
            // 
            this.infSkillToolStripMenuItem.Name = "infSkillToolStripMenuItem";
            this.infSkillToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.infSkillToolStripMenuItem.Text = "inf skill";
            this.infSkillToolStripMenuItem.Click += new System.EventHandler(this.InfSkillToolStripMenuItem_Click);
            // 
            // insanityModeToolStripMenuItem
            // 
            this.insanityModeToolStripMenuItem.CheckOnClick = true;
            this.insanityModeToolStripMenuItem.Name = "insanityModeToolStripMenuItem";
            this.insanityModeToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.insanityModeToolStripMenuItem.Text = "insanity mode";
            this.insanityModeToolStripMenuItem.Click += new System.EventHandler(this.InsanityModeToolStripMenuItem_Click);
            // 
            // infApplesToolStripMenuItem
            // 
            this.infApplesToolStripMenuItem.Name = "infApplesToolStripMenuItem";
            this.infApplesToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.infApplesToolStripMenuItem.Text = "inf apples";
            this.infApplesToolStripMenuItem.Click += new System.EventHandler(this.InfApplesToolStripMenuItem_Click);
            // 
            // giveMoneyToolStripMenuItem1
            // 
            this.giveMoneyToolStripMenuItem1.Name = "giveMoneyToolStripMenuItem1";
            this.giveMoneyToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.giveMoneyToolStripMenuItem1.Text = "give money";
            this.giveMoneyToolStripMenuItem1.Click += new System.EventHandler(this.GiveMoneyToolStripMenuItem1_Click);
            // 
            // givePotionsToolStripMenuItem
            // 
            this.givePotionsToolStripMenuItem.Name = "givePotionsToolStripMenuItem";
            this.givePotionsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.givePotionsToolStripMenuItem.Text = "give potions";
            this.givePotionsToolStripMenuItem.Click += new System.EventHandler(this.GivePotionsToolStripMenuItem_Click);
            // 
            // giveBombsToolStripMenuItem
            // 
            this.giveBombsToolStripMenuItem.Name = "giveBombsToolStripMenuItem";
            this.giveBombsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.giveBombsToolStripMenuItem.Text = "give bombs";
            this.giveBombsToolStripMenuItem.Click += new System.EventHandler(this.GiveBombsToolStripMenuItem_Click);
            // 
            // worldToolStripMenuItem
            // 
            this.worldToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setDayToolStripMenuItem,
            this.setNightToolStripMenuItem,
            this.freezeTimeToolStripMenuItem});
            this.worldToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.worldToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.worldToolStripMenuItem.Name = "worldToolStripMenuItem";
            this.worldToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.worldToolStripMenuItem.Text = "&World";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForUpdatesToolStripMenuItem,
            this.creditsToolStripMenuItem});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.checkForUpdatesToolStripMenuItem.Text = "check for updates";
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.CheckForUpdatesToolStripMenuItem_Click);
            // 
            // creditsToolStripMenuItem
            // 
            this.creditsToolStripMenuItem.Name = "creditsToolStripMenuItem";
            this.creditsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.creditsToolStripMenuItem.Text = "credits";
            this.creditsToolStripMenuItem.Click += new System.EventHandler(this.CreditsToolStripMenuItem_Click_1);
            // 
            // cursorholder
            // 
            this.cursorholder.BackColor = System.Drawing.Color.Transparent;
            this.cursorholder.BackgroundImage = global::CubeWorldTrainer.Properties.Resources.header;
            this.cursorholder.Controls.Add(this.label1);
            this.cursorholder.Controls.Add(this.label2);
            this.cursorholder.Dock = System.Windows.Forms.DockStyle.Top;
            this.cursorholder.Location = new System.Drawing.Point(0, 0);
            this.cursorholder.Name = "cursorholder";
            this.cursorholder.Size = new System.Drawing.Size(400, 52);
            this.cursorholder.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(365, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(25, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(343, 9);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "_";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // setDayToolStripMenuItem
            // 
            this.setDayToolStripMenuItem.Name = "setDayToolStripMenuItem";
            this.setDayToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.setDayToolStripMenuItem.Text = "set day";
            // 
            // setNightToolStripMenuItem
            // 
            this.setNightToolStripMenuItem.Name = "setNightToolStripMenuItem";
            this.setNightToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.setNightToolStripMenuItem.Text = "set night";
            // 
            // freezeTimeToolStripMenuItem
            // 
            this.freezeTimeToolStripMenuItem.CheckOnClick = true;
            this.freezeTimeToolStripMenuItem.Name = "freezeTimeToolStripMenuItem";
            this.freezeTimeToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.freezeTimeToolStripMenuItem.Text = "freeze time";
            this.freezeTimeToolStripMenuItem.Click += new System.EventHandler(this.FreezeTimeToolStripMenuItem_Click);
            // 
            // flylabel
            // 
            this.flylabel.AutoSize = true;
            this.flylabel.BackColor = System.Drawing.Color.Transparent;
            this.flylabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flylabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.flylabel.Location = new System.Drawing.Point(131, 362);
            this.flylabel.Name = "flylabel";
            this.flylabel.Size = new System.Drawing.Size(63, 13);
            this.flylabel.TabIndex = 117;
            this.flylabel.Text = "G: toggle fly";
            // 
            // flycheckbox
            // 
            this.flycheckbox.BackColor = System.Drawing.Color.Transparent;
            this.flycheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("flycheckbox.BackgroundImage")));
            this.flycheckbox.Checked = false;
            this.flycheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flycheckbox.Location = new System.Drawing.Point(355, 363);
            this.flycheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.flycheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.flycheckbox.Name = "flycheckbox";
            this.flycheckbox.Size = new System.Drawing.Size(23, 12);
            this.flycheckbox.TabIndex = 118;
            // 
            // freezetimecheckbox
            // 
            this.freezetimecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.freezetimecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("freezetimecheckbox.BackgroundImage")));
            this.freezetimecheckbox.Checked = false;
            this.freezetimecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.freezetimecheckbox.Location = new System.Drawing.Point(355, 341);
            this.freezetimecheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.freezetimecheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.freezetimecheckbox.Name = "freezetimecheckbox";
            this.freezetimecheckbox.Size = new System.Drawing.Size(23, 12);
            this.freezetimecheckbox.TabIndex = 116;
            // 
            // insanitymode
            // 
            this.insanitymode.BackColor = System.Drawing.Color.Transparent;
            this.insanitymode.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("insanitymode.BackgroundImage")));
            this.insanitymode.Checked = false;
            this.insanitymode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insanitymode.Location = new System.Drawing.Point(356, 122);
            this.insanitymode.MaximumSize = new System.Drawing.Size(23, 12);
            this.insanitymode.MinimumSize = new System.Drawing.Size(23, 12);
            this.insanitymode.Name = "insanitymode";
            this.insanitymode.Size = new System.Drawing.Size(23, 12);
            this.insanitymode.TabIndex = 98;
            // 
            // applecheck
            // 
            this.applecheck.BackColor = System.Drawing.Color.Transparent;
            this.applecheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("applecheck.BackgroundImage")));
            this.applecheck.Checked = false;
            this.applecheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.applecheck.Location = new System.Drawing.Point(356, 177);
            this.applecheck.MaximumSize = new System.Drawing.Size(23, 12);
            this.applecheck.MinimumSize = new System.Drawing.Size(23, 12);
            this.applecheck.Name = "applecheck";
            this.applecheck.Size = new System.Drawing.Size(23, 12);
            this.applecheck.TabIndex = 96;
            // 
            // skillcheck
            // 
            this.skillcheck.BackColor = System.Drawing.Color.Transparent;
            this.skillcheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("skillcheck.BackgroundImage")));
            this.skillcheck.Checked = false;
            this.skillcheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skillcheck.Location = new System.Drawing.Point(356, 101);
            this.skillcheck.MaximumSize = new System.Drawing.Size(23, 12);
            this.skillcheck.MinimumSize = new System.Drawing.Size(23, 12);
            this.skillcheck.Name = "skillcheck";
            this.skillcheck.Size = new System.Drawing.Size(23, 12);
            this.skillcheck.TabIndex = 95;
            // 
            // manacheck
            // 
            this.manacheck.BackColor = System.Drawing.Color.Transparent;
            this.manacheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("manacheck.BackgroundImage")));
            this.manacheck.Checked = false;
            this.manacheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.manacheck.Location = new System.Drawing.Point(356, 80);
            this.manacheck.MaximumSize = new System.Drawing.Size(23, 12);
            this.manacheck.MinimumSize = new System.Drawing.Size(23, 12);
            this.manacheck.Name = "manacheck";
            this.manacheck.Size = new System.Drawing.Size(23, 12);
            this.manacheck.TabIndex = 94;
            // 
            // godmodecheck
            // 
            this.godmodecheck.BackColor = System.Drawing.Color.Transparent;
            this.godmodecheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("godmodecheck.BackgroundImage")));
            this.godmodecheck.Checked = false;
            this.godmodecheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.godmodecheck.Location = new System.Drawing.Point(357, 59);
            this.godmodecheck.MaximumSize = new System.Drawing.Size(23, 12);
            this.godmodecheck.MinimumSize = new System.Drawing.Size(23, 12);
            this.godmodecheck.Name = "godmodecheck";
            this.godmodecheck.Size = new System.Drawing.Size(23, 12);
            this.godmodecheck.TabIndex = 93;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(400, 495);
            this.Controls.Add(this.centerpanel);
            this.Controls.Add(this.cursorholder);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CubeWorldTrainer";
            this.TransparencyKey = System.Drawing.Color.Orange;
            this.Load += new System.EventHandler(this.Window2_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.centerpanel.ResumeLayout(false);
            this.centerpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bombnumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.potionnumberic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumeric)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.cursorholder.ResumeLayout(false);
            this.cursorholder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip tooltip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel cursorholder;
        public System.Windows.Forms.Label error;
        public System.Windows.Forms.Label errorstatus;
        private System.Windows.Forms.Panel centerpanel;
        public System.Windows.Forms.CheckBox mute;
        private System.Windows.Forms.CheckBox disablehotkeyscheckbox;
        public System.Windows.Forms.NumericUpDown moneynumeric;
        private System.Windows.Forms.Label moneylabel;
        public System.Windows.Forms.Label hookedstatus;
        private System.Windows.Forms.Label infappleslabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label infintivegrenadeslabel;
        private System.Windows.Forms.Label infinitiveshieldlabel;
        private System.Windows.Forms.Label godmodelabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem giveMoneyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem worldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem godmodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infManaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infSkillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infApplesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giveMoneyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditsToolStripMenuItem;
        public System.Windows.Forms.Button moneybtn;
        public controls.CustomCheckBox godmodecheck;
        public controls.CustomCheckBox manacheck;
        public controls.CustomCheckBox skillcheck;
        public controls.CustomCheckBox applecheck;
        public controls.CustomCheckBox insanitymode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem insanityModeToolStripMenuItem;
        public System.Windows.Forms.Label aobresult;
        public System.Windows.Forms.Label aoblabel;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button potiongivebtn;
        public System.Windows.Forms.NumericUpDown potionnumberic;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem givePotionsToolStripMenuItem;
        public System.Windows.Forms.Button bombbtn;
        public System.Windows.Forms.NumericUpDown bombnumeric;
        private System.Windows.Forms.Label bomblabel;
        private System.Windows.Forms.ToolStripMenuItem giveBombsToolStripMenuItem;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox teleportbox;
        public System.Windows.Forms.Button teleporteditbtn;
        public System.Windows.Forms.Button tpbtn;
        public System.Windows.Forms.Button daybtn;
        private System.Windows.Forms.Label daytimelabel;
        public System.Windows.Forms.Button nightbtn;
        private System.Windows.Forms.Label nightlabel;
        public controls.CustomCheckBox freezetimecheckbox;
        private System.Windows.Forms.Label freezetimelabel;
        private System.Windows.Forms.ToolStripMenuItem setDayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setNightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem freezeTimeToolStripMenuItem;
        public controls.CustomCheckBox flycheckbox;
        private System.Windows.Forms.Label flylabel;
    }
}
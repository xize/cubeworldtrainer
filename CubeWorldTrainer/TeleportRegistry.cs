﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer
{
    class TeleportRegistry
    {

        private static TeleportRegistry registry;

        private HashSet<TeleportLocation> locations = new HashSet<TeleportLocation>();

        private String installdir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)+@"\0c3";

        public  void Add(TeleportLocation tl)
        {
            locations.Add(tl);
        }

        public void Remove(TeleportLocation tl)
        {
            locations.Remove(tl);
        }

        public TeleportLocation ValueOf(String name)
        {
            foreach(TeleportLocation t in locations)
            {
                if(t.GetName().ToLower() == name.ToLower())
                {
                    return t;
                }
            }
            return null;
        }

        public TeleportLocation[] Values()
        {
            return locations.ToArray();
        }

        private bool DoesInstallDirExists()
        {
            if(Directory.Exists(this.installdir) && Directory.Exists(this.installdir + "/cubeworldtrainer"))
            {
                return true;
            }

            return false;
        }

        public void LoadFromFile()
        {
            this.locations.Clear();

            if(this.DoesInstallDirExists())
            {
                if(File.Exists(this.installdir + "/cubeworldtrainer" +@"/locations.ini"))
                {
                    String[] lines = File.ReadAllLines(this.installdir + "/cubeworldtrainer" + @"/locations.ini");
                    foreach(String line in lines)
                    {
                        String[] data = line.Split('|');
                        String name = data[0];
                        float x = float.Parse(data[1]);
                        float y = float.Parse(data[2]);
                        float z = float.Parse(data[3]);
                        TeleportLocation l = new TeleportLocation(name, x, y , z);
                        this.locations.Add(l);
                    }
                }
            } else
            {
                //setup
                Directory.CreateDirectory(this.installdir);
                Directory.CreateDirectory(this.installdir + "/cubeworldtrainer");
            }
        }

        public void SaveToFile()
        {
            if(!Directory.Exists(this.installdir))
            {
                Directory.CreateDirectory(this.installdir);
            }

            if(!Directory.Exists(this.installdir+"/cubeworldtrainer"))
            {
                Directory.CreateDirectory(this.installdir+"/cubeworldtrainer");
            }

            StringBuilder builder = new StringBuilder();

            foreach(TeleportLocation tp in this.locations)
            {
                builder.AppendLine(tp.GetName()+"|"+tp.GetX()+"|"+tp.GetY()+"|"+tp.GetZ());
            }

            File.WriteAllText(this.installdir + "/cubeworldtrainer" + @"/locations.ini", builder.ToString());
        }


        public void RenameLocation(String newname, String oldname)
        {
            TeleportLocation l = ValueOf(oldname);

            float x = l.GetX();
            float y = l.GetY();
            float z = l.GetZ();

            this.locations.Remove(l);

            TeleportLocation newloc = new TeleportLocation(newname, x, y , z);
            this.locations.Add(newloc);
        }

        public static TeleportRegistry GetRegistry()
        {
            if(registry == null)
            {
                registry = new TeleportRegistry();
            }
            return registry;
        }

    }
}

﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeWorldTrainer.cheats
{
    abstract class ModelCheat
    {

        private String baseptr;
        private Mem m;
        public ModelCheat(String baseptr, Mem m)
        {
            this.baseptr = baseptr;
            this.m = m;
        }

        public String GetBaseAddress()
        {
            return this.baseptr;
        }

        public void SetBaseAddress(String b)
        {
            this.baseptr = b;
        }

        public Mem GetMemory()
        {
            return this.m;
        }

    }
}

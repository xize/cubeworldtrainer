﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeWorldTrainer.cheats
{
    public interface IFly
    {

        void Fly();

        Task LoadAOB();

    }
}

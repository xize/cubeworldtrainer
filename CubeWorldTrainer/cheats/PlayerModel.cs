﻿using CubeWorldTrainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CubeWorldTrainer.cheats
{
    class PlayerModel : ModelCheat, IPlayer
    {

        String AOBsig = "02 00 00 00 C8 99 DF 74 B1 0C CB 3F 13 09 20 1D 6D B0 C1 3F ?? ?? ?? ?? ?? ?? ?? ?? 54 99 b0 D0 F5 6C D7 3F 50 e9 2F 37 EF C6 D3 3F C8 C8 C8 00 01 00 00 00 ff ff ff 00 00 00 00 00 ff ff ff 00 02 00 00 00 C8 99 DF 74 B1 0C CB 3F 13 09 20 1D 6D B0 C1 3F ?? ?? ?? ?? ?? 00 00 00 00 00 00 00 00"; //add 14 bytes to get the correct offset.

        private bool isAOB = false;

        private float oldx;
        private float oldy;
        private float oldz;

        private int daytime = 1269843465;
        private int nighttime = 1232056480;
        private bool timefrozen = false;

        public PlayerModel(Mem m) : base("cubeworld.exe+0x00551E80", m){}

        public void DoGodmode()
        {
            String offsets = "0x2E0,0x220,0x78,0x10,0x10,0x1E8,0x248,0x180";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", Int16.MaxValue+"");
        }

        public void DoInfMana()
        {
            String offsets = "0x398,0x298,0x8,0x40,0x0,0x10,0x188";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "float", "1");
        }

        public void DoCrazyMana()
        {
            String offsets = "0x398,0x298,0x8,0x40,0x0,0x10,0x188";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", "1.0");
        }

        public void DoCrazyHealth()
        {
            String offsets = "0x2E0,0x220,0x78,0x10,0x10,0x1E8,0x248,0x180";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "float", Int32.MaxValue + "");
        }

        public void DoInfSkill()
        {
            String offsets = "0x2F0,0x210,0x8,0x40,0x0,0x10,0x9A0";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "float", "1");
        }

        public void DoInfApples()
        {
            String offsets = "0x2E0,0x220,0x78,0x240,0x40,0x70,0x1E8,0x0,0x30,0x290";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "int", "5");
        }

        public void GiveMoney(int amount)
        {
            String offsets = "0x2E0,0x218,0x40,0x78,0x1E8,0x248,0xAAC";
            int current = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", (current+amount)+"");
        }

        public void GivePotions(int amount)
        {
            String offsets = "0x2E0,0x220,0x40,0x78,0x1E8,0x0,0x30,0x0";
            int current = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "int", (current+amount)+"");
        }

        public bool GiveBombs(int amount)
        {
            String offsets = "0x1D0,0x48,0x8,0x8,0x10,0x78,0x1E8,0x0,0x30,0x1EC";
            int current = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);
            if((current+amount) > 50)
            {
                Console.Beep(100, 800);
                return false;
            }
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "int", (current+amount)+"");
            return true;
        }

        public float GetX()
        {
            String offsets = "0x2E0,0x238,0x40,0x78,0x1E8,0x248,0x18";
            float val = this.GetMemory().readFloat(this.GetBaseAddress()+","+offsets, "", false);
            return val;
        }

        public float GetY()
        {
            String offsets = "0x128,0x1E8,0x330,0x198,0x10,0x1E8,0x248,0x20";
            float val = this.GetMemory().readFloat(this.GetBaseAddress() + "," + offsets, "", false);
            return val;
        }

        public float GetZ()
        {
            String offsets = "0x340,0x1E8,0x368,0x2B8,0x6D8,0x8,0x448,0x10";
            float val = this.GetMemory().readFloat(this.GetBaseAddress() + "," + offsets, "", false);
            return val;
        }

        public void TeleportTo(float x, float y, float z)
        {
            //store old location before teleporting
            this.oldx = GetX();
            this.oldy = GetY();
            this.oldz = GetZ();

            String xoffset = "0x2E0,0x238,0x40,0x78,0x1E8,0x248,0x18";
            String yoffset = "0x128,0x1E8,0x330,0x198,0x10,0x1E8,0x248,0x20";
            String zoffset = "0x340,0x1E8,0x368,0x2B8,0x6D8,0x8,0x448,0x10";

            this.GetMemory().writeMemory(this.GetBaseAddress()+","+xoffset, "float", x+"");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + yoffset, "float", y + "");
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + zoffset, "float", z + "");
        }

        public void TeleportUndo()
        {
            if(oldx != 0 && oldy != 0 && oldz != 0)
            {
                this.TeleportTo(oldx, oldy, oldz);
            }
        }

        public async Task LoadAOB()
        {

            long t = (await this.GetMemory().AoBScan(this.AOBsig, true, false)).FirstOrDefault();

            if(t == 0)
            {
                return;
            }

            long a = t + 0x14;

            String address = Convert.ToString(a, 16);

            if(address != "0x0")
            {
                this.isAOB = true;
                Console.Beep();
                this.SetBaseAddress(address.ToUpper());
                Console.Beep();
                return;
            }

            Console.Beep(100, 800);
        }

        public void SetDay()
        {
            String offsets = "0x1D0,0x90,0x10,0x48,0x8,0x8,0x10,0x70,0x210,0x6FC";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "int", this.daytime+"");
        }

        public void SetNight()
        {
            String offsets = "0x1D0,0x90,0x10,0x48,0x8,0x8,0x10,0x70,0x210,0x6FC";
            this.GetMemory().writeMemory(this.GetBaseAddress() + "," + offsets, "int", this.nighttime + "");
        }

        public void FreezeTime()
        {
            if(this.timefrozen)
            {
                return;
            }
            this.timefrozen = true;
            String offsets = "0x1D0,0x90,0x10,0x48,0x8,0x8,0x10,0x70,0x210,0x6FC";
            int time = this.GetMemory().readInt(this.GetBaseAddress()+","+offsets);

            this.GetMemory().FreezeValue(this.GetBaseAddress()+","+offsets, "int", time+"");

        }

        public void UnFreezeTime()
        {
            String offsets = "0x1D0,0x90,0x10,0x48,0x8,0x8,0x10,0x70,0x210,0x6FC";
            this.timefrozen = false;
            this.GetMemory().UnfreezeValue(this.GetBaseAddress()+","+offsets);
        }

        public bool IsFrozen()
        {
            return this.timefrozen;
        }

        public bool IsAOB()
        {
            return this.isAOB;
        }
    }
}

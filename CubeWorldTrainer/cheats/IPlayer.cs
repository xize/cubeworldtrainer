﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeWorldTrainer.cheats
{
    public interface IPlayer
    {

        void DoGodmode();

        void DoInfMana();

        void DoCrazyMana();

        void DoCrazyHealth();

        void DoInfSkill();

        void DoInfApples();

        void GiveMoney(int amount);

        void GivePotions(int amount);

        bool GiveBombs(int amount);

        Task LoadAOB();

        void TeleportTo(float x, float y, float z);

        void TeleportUndo();

        float GetX();

        float GetY();

        float GetZ();

        void SetDay();

        void SetNight();

        void FreezeTime();

        void UnFreezeTime();

        bool IsFrozen();

        bool IsAOB();

    }
}

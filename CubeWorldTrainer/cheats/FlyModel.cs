﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeWorldTrainer.cheats
{
    class FlyModel : ModelCheat, IFly
    {

        private String aobsig = "?? ?? ?? ?? ?? 02 00 00 00 00 00 00 00 00 00 00 ?? ?? ?? ?? ?? 02 00 00 ?? ?? ?? ?? ?? 02 00 00 ?? ?? D7 D7 03 02 00 00 D8 53 4E C6 03 02 00 00 58 60 4E C6 03 02 00 00 18 B0";
        private bool isAOB = false;

        public FlyModel(Mem mem) : base("cubeworld.exe+0x00551A80", mem) {}

        public void Fly()
        {
            String offsets = "0x18,0x300,0x28,0x8,0x3C8,0x8,0x28,0x8,0x58,0x3C";
            this.GetMemory().writeMemory(this.GetBaseAddress()+","+offsets, "float", 10+"");
        }

        public async Task LoadAOB()
        {

            long t = (await this.GetMemory().AoBScan(this.aobsig, true, false)).FirstOrDefault();

            String address = Convert.ToString(t, 16);

            if(address != "0x0")
            {
                this.SetBaseAddress(address.ToUpper());
                Console.Beep();
                return;
            }
            Console.Beep(100, 800);
        }

        public bool IsAOB()
        {
            return this.isAOB;
        }

    }
}
